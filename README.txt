Proyecto de programaci�n de sistemas.
...........................
Hay 2 branches:
- master: rama principal
- working: cosas sin acabar que podr�an hacer que el proyecto dejase de funcionar.

Mandamientos:
1 - No hagas clone del repositorio. Haz fork.
2 - git checkout working.
3 - Nunca, nunca, nunca trabajes desde master.
4 - Recuerda hacer commit antes de empezar a hacer cambios.
5 - Los commit s�lo ser�n cuando algo funcione. Si est� a medias/no funciona. NO lo hagas.
6 - Pushea con precauci�n. NUNCA algo con fallos (de haber algo con fallos, es porque no se ha respetado el punto 5).
7 - No cumplir mandamientos --> Golpe de remo muy fuerte.
