package lz77.JUnits;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import lz77.Decompressor;

import org.junit.Test;

/**
 * Comprueba el funcionamiento correcto de las combinaciones de argumentos que puede recibir main.
 * 
 * @author Aitor
 * 
 */
public class MainTest {

	@Test
	public void test() throws Exception {
		String[] test1 = { "file.txt" };
		String[] test2 = { "file.txt", "fileO.txt" };
		String[] test3 = { "file.txt", "fileO2.txt", "512" };
		String[] test4 = { "file.txt", "fileO2.txt", "512", "arg4" };

		Path p = Paths.get(test1[0]);
		System.out.println("Ruta: " + p.toUri());
		if (Files.exists(p)) {
			Files.delete(p);
		}
		Files.createFile(p);
		// PRUEBA DE ARGUMENTOS
		System.out.println("Prueba con un argumentos:");
		try {
			Decompressor.main(test1);
			System.out.println("Correcto.");
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Prueba con dos argumentos:");
		try {
			Decompressor.main(test2);
			System.out.println("Correcto.");
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Prueba con tres argumentos:");
		try {
			Decompressor.main(test3);
			System.out.println("Correcto.");
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		System.out.println("Prueba con cuatro argumentos (esperado \"exeption\"): ");
		try {
			Decompressor.main(test4);
		} catch (Exception e) {
			System.out.println("Correcto.");
		}

		if (Files.exists(p)) {
			Files.delete(p);
		}
	}

}
