package lz77.JUnits;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.file.Files;

import lz77.utils.FileManager;


import org.junit.Test;



public class FileTest {

	@Test
	public void test() throws IOException {
		
		//TODO: crear fichero de path1 SI NO existe
		
		// Fichero que existe.
		String path1 = "prueba.txt";
		FileManager fm = new FileManager(path1);
		
		if(Files.exists(fm.getFilePath())){
			System.out.println("Path1 correcta");
			System.out.println(fm.getFilePath().toUri());
			
		}else{
			fail("File " + path1 + " should exist");
		}
		
		//TODO: crear fichero que ya existe ( usar path1)
		
//		TODO: borrar fichero que ya existe
		
		// Fichero que no existe.
		String path2 = "noexiste.txt";
		fm = new FileManager(path2);
		if(!Files.exists(fm.getFilePath())){
			System.out.println("Path2 no existe (correcta)");		
		}else{
			fail("File " + path2 + " shouldn't exist");
		}
		
		//TODO: borrar fichero que no existe
		
	}

}
