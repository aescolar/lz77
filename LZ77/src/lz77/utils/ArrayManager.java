package lz77.utils;

public abstract class ArrayManager {
	/**
	 * Returns true if the index is between 0 and array.lenght
	 * 
	 * @param value
	 * @param arrayI
	 * @return
	 */
	public static boolean isValidIndex(int index, String[] arrayS) {
		return (index >= 0 && index < arrayS.length);
	}

	/**
	 * Returns true if the index is from 0 to array.lenght
	 * 
	 * @param value
	 * @param arrayI
	 * @return
	 */
	public static boolean isValidIndex(int index, int[] arrayI) {
		return (index >= 0 && index < arrayI.length);
	}

	/**
	 * Returns true if the index is from 0 to array.lenght
	 * 
	 * @param value
	 * @param arrayI
	 * @return
	 */
	public static boolean isValidIndex(int index, boolean[] arrayB) {
		return (index >= 0 && index < arrayB.length);
	}

	/**
	 * Gets the index for the value to find. Returns -1 if the value is not found.
	 * 
	 * @param value
	 * @param arrayI
	 * @return
	 */
	public static int getArrayIndex(int value, int[] arrayI) {
		int index = -1;
		boolean found = false;
		for (int i = 0; i < arrayI.length && !found; i++) {
			if (value == arrayI[i]) {
				index = i;
				found = true;
			}
		}
		return index;
	}

	/**
	 * Gets the index for the value to find. Returns -1 if the value is not found.
	 * 
	 * @param value
	 * @param arrayI
	 * @return
	 */
	public static int getArrayIndex(boolean value, boolean[] arrayB) {
		int index = -1;
		boolean found = false;
		for (int i = 0; i < arrayB.length && !found; i++) {
			if (value == arrayB[i]) {
				index = i;
				found = true;
			}
		}
		return index;
	}

	/**
	 * Returns the value for the index
	 * 
	 * @param value
	 * @param arrayI
	 * @return
	 */
	public static String getArrayValue(int index, String[] arrayS) {
		return arrayS[index];
	}

	/**
	 * Returns the value for the index
	 * 
	 * @param value
	 * @param arrayI
	 * @return
	 */
	public static int getArrayValue(int index, int[] arrayI) {
		return arrayI[index];
	}

	/**
	 * Returns the value for the index
	 * 
	 * @param value
	 * @param arrayI
	 * @return
	 */
	public static boolean getArrayValue(int index, boolean[] arrayB) {
		return arrayB[index];
	}

	
	public static boolean searchIgnoreCase(String value, String[] arrayS) {
		boolean found = false;
		// TODO implement this method
		return found;
	}

	public static boolean search(String value, String[] arrayS) {
		boolean found = false;
		// TODO implement this method
		return found;
	}

	public static boolean search(int value, int[] arrayI) {
		boolean found = false;
		// TODO implement this method
		return found;
	}

	public static String toString(String[] arrayS) {
		return toTransposableString(arrayS, 'H');
	}

	public static String toString(int[] arrayI) {
		return toTransposableString(arrayI, 'H');
	}

	public static String toString(boolean[] arrayB) {
		return toTransposableString(arrayB, 'H');
	}

	public static String toTransposableString(String[] arrayS, char type) {
		String result = "";
		// TODO implement this method
		return result;
	}

	public static String toTransposableString(int[] arrayI, char type) {
		String result = "";
		// TODO implement this method
		return result;
	}

	public static String toTransposableString(boolean[] arrayB, char type) {
		String result = "";
		// TODO implement this method
		return result;
	}

	public static void print(String[] arrayS) {
		System.out.println(toString(arrayS));
	}

	public static void print(int[] arrayI) {
		System.out.println(toString(arrayI));
	}

	public static void print(boolean[] arrayB) {
		System.out.println(toString(arrayB));
	}

	public static void transposablePrint(String[] arrayS, int type) {
		switch (type) {
		case 'H':
		case 'h':
			System.out.println(toTransposableString(arrayS, 'H'));
			break;
		case 'V':
		case 'v':
			System.out.println(toTransposableString(arrayS, 'V'));
			break;
		}
	}

	public static void transposablePrint(int[] arrayI, int type) {
		switch (type) {
		case 'H':
		case 'h':
			System.out.println(toTransposableString(arrayI, 'H'));
			break;
		case 'V':
		case 'v':
			System.out.println(toTransposableString(arrayI, 'V'));
			break;
		}
	}

	public static void transposablePrint(boolean[] arrayB, int type) {
		switch (type) {
		case 'H':
		case 'h':
			System.out.println(toTransposableString(arrayB, 'H'));
			break;
		case 'V':
		case 'v':
			System.out.println(toTransposableString(arrayB, 'V'));
			break;
		}
	}

	public static int getArrayIndex(String value, String[] arrayS) {
		int index = -1;
		boolean found = false;
		for (int i = 0; i < arrayS.length && !found; i++) {
			if (value.equalsIgnoreCase(arrayS[i])) {
				index = i;
				found = true;
			}
		}
		return index;
	}

	public static boolean setArrayValue(int index, String value, String[] arrayS) {
		boolean isSet = false;
		if (isValidIndex(index, arrayS)) {
			arrayS[index] = value;
			isSet = true;
		}
		return isSet;
	}

	public static boolean setArrayValue(int index, int value, int[] arrayI) {
		boolean isSet = false;
		if (isValidIndex(index, arrayI)) {
			arrayI[index] = value;
			isSet = true;
		}
		return isSet;
	}

	public static boolean setArrayValue(int index, boolean value, boolean[] arrayB) {
		boolean isSet = false;
		if (isValidIndex(index, arrayB)) {
			arrayB[index] = value;
			isSet = true;
		}
		return isSet;
	}

	public static boolean checkArrayValue(int index, String value, String[] arrayS) {
		boolean result = false;
		// TODO implement this method
		return result;
	}

	public static boolean checkArrayValue(int index, int value, int[] arrayI) {
		boolean result = false;
		// TODO implement this method
		return result;
	}

	public static boolean checkArrayValue(int index, boolean value, boolean[] arrayB) {
		boolean result = false;
		// TODO implement this method
		return result;
	}

	public static String[] resetArray(String[] arrayS, String value) {
		for (int i = 0; i < arrayS.length; i++) {
			arrayS[i] = value;
		}
		return arrayS;
	}

	public static int[] resetArray(int[] arrayI, int value) {
		for (int i = 0; i < arrayI.length; i++) {
			arrayI[i] = value;
		}
		return arrayI;
	}

	public static boolean[] resetArray(boolean[] arrayB, boolean value) {
		for (int i = 0; i < arrayB.length; i++) {
			arrayB[i] = value;
		}
		return arrayB;
	}

}
