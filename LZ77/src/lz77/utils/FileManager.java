package lz77.utils;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileManager {
	
	private Path filePath;
	
	public FileManager(String path){
		setFilePath(Paths.get(path));
	}

	public Path getFilePath() {
	   return filePath;
   }

	public void setFilePath(Path filePath) {
	   this.filePath = filePath;
   }

	public static String[] file2arrayS(String file, int size) throws IOException {
		String[] outputArray = new String[size];
		//TODO: implement this method
		return outputArray;
	}

	public static int[] file2arrayI(String file, int size) throws IOException {
		int[] outputArray = new int[size];
		//TODO implement this method
		return outputArray;
	}

	public static boolean[] file2arrayB(String file, int size) throws IOException {
		boolean[] outputArray = new boolean[size];
	//TODO implement this method
		return outputArray;
	}

	public static int countLines(String file){
		int numLines = 0;
	//TODO implement this method		}
		return numLines;

	}

	public static void array2File(String[] arrayS, String file) throws IOException {
	//TODO implement this method
	}
	
	public static void array2File(int[] arrayI, String file) throws IOException {
	//TODO implement this method
	}
	
	public static void array2File(boolean[] arrayB, String file) throws IOException {
	//TODO implement this method
	}

	

}


