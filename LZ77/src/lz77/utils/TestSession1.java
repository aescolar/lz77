package lz77.utils;

public class TestSession1 {
	public static void main(String[] args) {
		String[] names = { "Maria", "Pedro", "Antonio" };
		String[] surnames = { "Mart�n", "P�rez", "Alonso" };
		int[] ages = { 35, 20, 40 };
		boolean[] isMarried = { true, false, false };

		System.out.println("***TEST isValidIndex for String array***");
		System.out.println("test isValidIndex(). Expected: " + names.length
				+ ". Output for length + 1:"
				+ ArrayManager.isValidIndex(names.length + 1, names));
		System.out.println("test isValidIndex(). Expected: " + names.length
				+ ". Output for length - 1:"
				+ ArrayManager.isValidIndex(names.length - 1, names));

		System.out.println("***TEST isValidIndex for int array***");
		System.out.println("test isValidIndex(). Expected: " + ages.length
				+ ". Output for length + 1:"
				+ ArrayManager.isValidIndex(ages.length + 1, ages));
		System.out.println("test isValidIndex(). Expected: " + ages.length
				+ ". Output for length - 1:"
				+ ArrayManager.isValidIndex(ages.length - 1, ages));

		System.out.println("***TEST isValidIndex for boolean array***");
		System.out.println("test isValidIndex(). Expected: " + isMarried.length
				+ ". Output for length + 1:"
				+ ArrayManager.isValidIndex(isMarried.length + 1, isMarried));
		System.out.println("test isValidIndex(). Expected: " + isMarried.length
				+ ". Output for length - 1:"
				+ ArrayManager.isValidIndex(isMarried.length - 1, isMarried));
		
		System.out.println("***TEST getArrayIndex for String array***");
		System.out.println("test isValidIndex(). Expected: 2" 
				+ ". Output for \"P�rez\" (surnames[2]):" + ArrayManager.getArrayIndex(surnames[2], surnames));
		System.out.println("test isValidIndex(). Expected: -1" 
				+ ". Output for \"P�rez\" (surnames[2]):" + ArrayManager.getArrayIndex("Galileo", surnames));

		// TODO: Test all the methods of FileManager reading (from file to
		// array) and writing (from array to file) the array content. Ask your
		// teacher for some explanation about IO in java

		// TODO: Remove array initialization and initialize the arrays using the
		// content of the files names.txt, surnames.txt, ages.txt and
		// isMarried.txt. that contains one array element per line

		// TODO: Create three instances of the class Student using as attributes
		// value the content read from arrays. You will need to create class
		// Student in a new File (Student.java)
	}

}
