package lz77;

import java.nio.file.Files;

import lz77.utils.FileManager;

public class Decompressor {
	public final static boolean DEBUG_MODE = true;
	public static int BUFFER_SIZE = 65536;

	static FileManager fileManagerIn;
	static FileManager fileManagerOut;

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		if (args.length < 1 || args.length > 3) {
			throw new Exception("Expected <input file name> [output file name] [buffer size]");
		} else {
			if (args.length >= 1) {
				// TODO caso ficheroEntrada
				fileManagerIn = new FileManager(args[0]);
				if(!Files.isReadable(fileManagerIn.getFilePath())){
					throw new Exception("Not a readable file."); 
				}

				if (!Files.exists(fileManagerIn.getFilePath())) {
					throw new Exception("El fichero a descomprimir no existe.");
				}

				if (args.length >= 2) {
					// TODO caso ficheroEntrada ficheroSalida
					fileManagerOut = new FileManager(args[1]);

					if (args.length == 3) {
						// TODO caso ficheroEntrada ficheroSalida tamañoBuffer
						createBuffer(Integer.parseInt(args[2]));
					}
				}
			}
		}

	}

	public static void createBuffer(int Buffer_size) {
		BUFFER_SIZE = Buffer_size;
		log("Buffer creado: " + BUFFER_SIZE);
	}

	public static void log(String message) {
		System.out.println(message);
	}

}
